const config = require('config');
const dbConfig = config.get('DB_CONFIG');
const connectionString = `postgresql://${dbConfig.USERNAME}:${dbConfig.PASSWORD}@${dbConfig.HOSTNAME}:${dbConfig.PORT}/${dbConfig.DATABASE}`

const {
  Pool,
  Client
} = require('pg');


// Cytokine
const processsFileCytokine = async function (fileType, outFile) {
  console.log("fileType--->" + fileType);
  console.log("outFile--->" + outFile);
  
  var jobId = outFile.substr(0, outFile.lastIndexOf("."));

  const client = new Client({
    connectionString: connectionString,
  })
  client.connect();
  // client.query('SELECT * from assay where assay_id =456', (err, res) => {
  //   console.log(err, res);
  //   client.end();
  // });
  client.query(`select * from TestInsert(1)`, (err, res) => {
    //console.log(err, res);
    client.end();
    return "done";
  });

}

const importData = async function (fileType, outFile) {
  console.log("fileType--->" + fileType);
  console.log("outFile--->" + outFile);
  
  var jobId = outFile.substr(0, outFile.lastIndexOf("."));

  const client = new Client({
    connectionString: connectionString,
  })
  client.connect();
  if(fileType.toUpperCase() == "CLINICAL"){
    fileType = "SUBJECT";
  }
  let sql = `select import_data('${fileType}','${outFile}','${jobId}')`;
  client.query(importData, (err, res) => {
    //console.log(err, res);
    client.end();
    return "done";
  });

}

const dbCSVController = {
  'processsFileCytokine': processsFileCytokine,
  'importData': importData
};

module.exports = dbCSVController;