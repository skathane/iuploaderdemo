var fs = require('fs');

var readFile = function(filePath, encoding, cb){
  let response = {'ack': '','message': '' };
  fs.stat(filePath, function(err, stat) {
    if(err && err.code == 'ENOENT') {         // file does not exist
      response.ack = 'fail';
      response.message = 'File not found';
      cb(response, null);
    }else if(err){
      response.ack = 'fail';
      response.message = err;
      cb(response, null);
    }else{
      fs.readFile(filePath, encoding, (err, result) => {
        (err) ? cb(err, null) : cb(null, result);
      });
    }
  });
}


var writeFile = function(filePath, fileData, cb){
  fs.writeFile(filePath, fileData, (err) => {
    (err) ? cb(err, null) : cb(null, `File written successfully`);
  });
}


var moveFile = function(sourceFilePath, destinationFilePath, cb){
  let response = {'ack': '','message': '' };

  fs.rename(sourceFilePath, destinationFilePath, function (err) {    
    if(err){
      response.ack = 'fail';
      response.message = err;
      cb(response, null)
    }else{
      response.ack = 'success';
      response.message = `File moved successfully.`;
      cb(null, response);
    }
  })
}

var FileUtilityService = {
  'readFile': readFile,
  'writeFile': writeFile,
  'moveFile': moveFile
}
module.exports = FileUtilityService;
