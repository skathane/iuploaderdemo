var async = require('async');
var config = require('config');
var fileUtil = require(`../utils/FileUtil`);
var path = require('path');
//var CsvController = require(`../controllers/CsvController`);
const fs = require('fs');
const csv = require('fast-csv');
const dbConfig = config.get('DB_CONFIG');
const connectionString = `postgresql://${dbConfig.USERNAME}:${dbConfig.PASSWORD}@${dbConfig.HOSTNAME}:${dbConfig.PORT}/${dbConfig.DATABASE}`
var getApiInstance = require('../routes/apiInstance');
const querystring = require('querystring');
const axios = require('axios');
var apiInstance;
const {
  Pool,
  Client
} = require('pg');


/**
 * To request new job.
 */
var createJob = function (job) {
  var jobDetails = job;

  async.waterfall([
    function (cb) {
      getServiceQueue(function (err, result) {
        let jobList = [];
        if (err && err.message && err.message != 'File not found') {
          cb(err, null);
        }
        if (err && err.message && err.message == 'File not found') {
          jobList.push(jobDetails);
        } else {
          if (result) {
            jobList = JSON.parse(result);
          }
          jobList.push(jobDetails);
        }
        createOrUpdateJobQueueFile(jobList, cb);
      });
    }
  ], function (err, result) {
    if (err) {
      console.log(err);
    }
  });
}

async function getToken() {
  let token = null;
  try {
    let res = await axios.post(config.get('API_URL') + '/api/token', querystring.stringify(config.get('SERVICE_USER')));
    if (res.data.hasOwnProperty('token')) {
      token = "Bearer " + res.data.token;
    }
  } catch (err) {
    token = null;
    console.log("Error in token generation");
  }
  return token;
}

async function clearCache(){
  let token = await getToken();
  if (token) {
    try {
      apiInstance = getApiInstance(token);
      let res = await apiInstance.delete(config.get('API_URL') + '/api/v1/setup/cache');
      console.log("Clear cache process done");
      //console.log(res);
    } catch (err) {
      token = null;
      console.log("Error in clear cache");
      console.log(err);
    }
  } else {
    console.log("Token is not generated");
  }
} 
var getServiceQueue = function (cb) {
  fileUtil.readFile(config.JOB_QUEUE_PATH, 'utf8', (err, result) => {
    (err) ? cb(err, null) : cb(null, result);
  });
}

var createOrUpdateJobQueueFile = function (jobDetails, cb) {
  let filePath = config.JOB_QUEUE_PATH;
  fileUtil.writeFile(filePath, JSON.stringify(jobDetails), function (err, result) {
    if (err) {
      cb(err, null);
    } else {
      cb(null, cb);
    }
  });
}

var getJob = function () {
  let jobRequest = getJobToProcess();
}

var getJobToProcess = function () {
  let jobList = [];
  async.waterfall([
    function (cb) {
      getServiceQueue(function (err, result) {
        (err) ? cb(err, null) : cb(null, result);
      });
    },
    function (result, cb) {
      let jobList = JSON.parse(result);

      let inprocessJobs = jobList.filter(function (obj) {
        return (obj.status == 'In-process');
      });

      let tempJobs = jobList.filter(function (obj) {
        return (obj.status == 'Uploaded' && obj.fileType !== 'FCS' && obj.fileType !== 'OTHER');
      });

      if (tempJobs.length > 0) {
        tempJobs = tempJobs.sort((a, b) => parseFloat(a.startTime) - parseFloat(b.startTime));
        let jobId = null;
        for (let i = 0; i < tempJobs.length; i++) {
          for (let j = 0; j < inprocessJobs.length; j++) {
            if (tempJobs[i].fileType !== inprocessJobs[j].fileType) {
              jobId = tempJobs[i].jobId;
              break;
            }
          }
        }
        if (jobId) {
          cb(null, jobList, jobId);
        } else {
          cb(null, jobList, tempJobs[0].jobId);
        }
      } else {
        cb(new Error('NO_JOB_TO_PROCESS'), null);
      }
    },
    function (jobList, jobId, cb) {
      let oJob = null;
      for (let i = 0; i < jobList.length; i++) {
        if (jobList[i].jobId == jobId) {
          oJob = jobList[i];
          jobList[i].status = "In-process";
          break;
        }
      }
      createOrUpdateJobQueueFile(jobList, function (err, result) {
        let outputCSVPath = config.CSV_FILE_OUTPUT_PATH;
        if (!fs.existsSync(outputCSVPath)){
          fs.mkdirSync(outputCSVPath);
        }
        if (oJob) {
          let inputFile = oJob.inputFilePath;
          let outputFile = config.get("CSV_FILE_OUTPUT_PATH") + "/" + oJob.jobId + ".csv";
          let fileType = oJob.fileType;

          if (fileType == 'CYTOKINE') {
            convertFileCytokine(oJob.userId, inputFile, outputFile);
          } else if (fileType == 'CLINICAL') {
            convertFileClinical(oJob.userId, inputFile, outputFile);
          } else if (fileType == 'IFC') {
            convertFileIFC(oJob.userId, inputFile, outputFile);
          } else if (fileType == 'NANOSTRING') {
            convertFileNanostring(oJob.userId, inputFile, outputFile);
          } else if (fileType == 'IHC') {
            convertFileIhc(oJob.userId, inputFile, outputFile);
          } else if (fileType == 'FLOW CYTOMETRY') {
            convertFileCytometry(oJob.userId, inputFile, outputFile);
          } else if (fileType == 'RADIOLOGY') {
            convertFileRadiology(oJob.userId, inputFile, outputFile);
          }
        } else {
          cb(null, result);
        }
      });
    }
  ], function (err, result) {
    if (err) {
      if (err.message == 'NO_JOB_TO_PROCESS') {
        console.log('No jobs to process.');
      } else {
        console.log(err);
      }
    }
    return result;
  });
}

var updateJobStatus = function (jobId, status, msg) {
  async.waterfall([
    function (cb) {
      getServiceQueue(function (err, result) {
        (err) ? cb(err, null) : cb(null, result);
      });
    },
    function (result, cb) {
      let jobList = JSON.parse(result);
      for (let i = 0; i < jobList.length; i++) {
        if (jobList[i].jobId == jobId) {
          oJob = jobList[i];
          jobList[i].status = status;
          jobList[i].errorDetail = msg;
          break;
        }
      }
      createOrUpdateJobQueueFile(jobList, cb);
    }
  ], function (err, result) {
    if (err) {
      console.log(err);
    }
    return result;
  });
}


var getJobs = function (req, res) {
  let searchString = req.query.search;
  let start = req.query.start? req.query.start : 0;
  let limit = req.query.limit ? req.query.limit : 10;
  let userId = req.query.userId;
  let userName = req.query.userName;
  let orderByCol = req.query.orderByCol;
  let sortDirection = req.query.sortDirection;
  let jobType = req.query.type;

  getServiceQueue(function (err, result) {
    if (err) {
      return res.json({ 'ack': 'fail', 'data': err });
    } else {
      let jobList = JSON.parse(result);
      let responseData = [];
      let filteredData = jobList.filter(function (obj) {
        return (obj.userId == userId);
      });

      filteredData = filteredData.sort((a, b) => parseFloat(b.startTime) - parseFloat(a.startTime)); // Sort descending by 'Start Time'

      // Pagination
      let perPage = parseInt(limit);
      let currentPage = parseInt(start);

      let fromRecord = ((currentPage * perPage) - perPage) + 1;
      let toRecord = (fromRecord + perPage) - 1;

      let startRec = fromRecord - 1;
      let lastPage = 0;

      if (currentPage == lastPage) {
        toRecord = filteredData.length;
      }
      if (filteredData.length) {
        lastPage = Math.ceil((filteredData.length) / perPage);
      }

      for (let i = startRec; (i < filteredData.length) && (i < (parseInt(startRec) + parseInt(limit))); i++) {
        let showLog = false;
        let isAllowExecute = false;
        filteredData[i].startTime = (new Date(Number(filteredData[i].startTime))).toLocaleString();
        if (filteredData[i].status.toUpperCase() == "FAILED" || filteredData[i].status.toUpperCase() == "PARTIAL SUCCESS") {
          showLog = true;
        }
        if (filteredData[i].fileType.toUpperCase() == "FCS"){
          isAllowExecute = true;
        }
        filteredData[i].isAllowExecute = isAllowExecute;
        filteredData[i].showLog = showLog;
        responseData.push(filteredData[i]);
      }

      let returnObj = {
        links: {
          'pagination': {
            'total': filteredData.length,
            'per_page': perPage,
            'current_page': currentPage,
            'last_page': lastPage,
            'next_page_url': null,
            'prev_page_url': null,
            'from': fromRecord,
            'to': toRecord
          }
        },
        data: responseData,
        totalCount: filteredData.length
      };

      //return res.json({ 'ack': 'success', 'totalCount': filteredData.length, 'data': responseData });
      return res.json(returnObj);
    }
  });
}



//CSV Controller
const CsvController = new class {
  constructor() {
  }
  parseCsvFile(filePath, inheader) {
    return new Promise((resolve, reserr) => {
      let returnList = [];
      const stream = fs.createReadStream(filePath);
      stream.on('error', function (err) {
        reserr(err)
      });
      csv
        .parseStream(stream, {
          headers: true,
          headers: inheader,
          renameHeaders: true,
          ignoreEmpty: true
        })
        .on('error', (error) => {
          console.error(error)
          reserr(error)
        })
        .on('data', (row) => {
          returnList.push(row);
        })
        .on('end', (rowCount) => {
          console.log(`Parsed ${rowCount} rows`);
          resolve(returnList);
        })
    });
  }

  saveCsvFile(filePath, rows, outorder) {
    return new Promise((resolve, reserr) => {
      let returnList = [];
      const stream = fs.createWriteStream(filePath);
      stream.on('error', function (err) {
        reserr(err);
      });

      csv
        .writeToStream(stream, rows, {
          headers: true,
          headers: outorder
        })
        .on('error', (error) => {
          console.error(error);
          reserr(error);
        })
        .on('finish', () => {
          console.log('Done writing.');
          resolve();
        })
    });
  }
  // Get cytokine row
  getCytokineRow(row) {
    var obj = {
      'STUDY ID': '', 'PHASE': '', 'ARM': '', 'TIMEPOINT': '', 'SAMPLE BARCODE': '', 'SUBJECT ID': '', 'PROCEDURE DATE': '', 'PANEL': '', 'MARKER': '', 'MEASURE': '', 'RESULT': '', 'RESULT UNIT': '', 'POPULATION': '', 'TMM': '', 'MRN': '', 'cyclic_loess': '', 'oracle': '', 'HG': '', 'log_transformation': '', 'CQN': '', 'RLOG': '', 'min_max': '', 'zscore': '', 'STPM': '', 'FPKM': '', 'RPKM': '', 'RC': '', 'UQ': '', 'median': '', 'RSEM': '', 'DEGES': '', 'loggeomeans': '', 'count': '', 'log10': '', 'mean_centering': '', 'mean_standardization': '', 'predose_ratio': '', 'median_ratio': '', 'cycle_pre_per_post': '', 'cycle': '', 'median_op_pg_per_ml_cp_': '', 'median_ratio_response': '', 'median_pgml_response': ''
    };
    Object.keys(row).forEach(item =>
      obj[item] = row[item]
    );
    return obj;
  }

  // Get IFC row
  getIFCRow(row) {
    var obj = {
      'STUDY ID': '', 'PHASE': '', 'ARM': '', 'TIMEPOINT': '', 'SUBJECT ID': '', 'SPECIMEN': '', 'Slide  Label ': '', 'Slide ID': '', 'Tissue Category': '', 'TISSUE AREA (mm2)': '', 'MARKER ': '', 'MEASURE': '', 'GRADE': '', 'RESULT': '', 'RESULT UNIT': '', 'TMM': '', 'MRN': '', 'cyclic_loess': '', 'oracle': '', 'HG': '', 'log_transformation': '', 'CQN': '', 'RLOG': '', 'min_max': '', 'zscore': '', 'STPM': '', 'FPKM': '', 'RPKM': '', 'RC': '', 'UQ': '', 'median': '', 'RSEM': '', 'DEGES': '', 'loggeomeans': '', 'count': '', 'log10': '', 'mean_centering': '', 'mean_standardization': '', 'cd134_plus_number_per_mmsq': '', 'cd134l_plus_number_per_mmsq': '', 'cd16_minus_cd56_plus_cd134_plus_number_per_mmsq': '', 'cd16_minus_cd56_plus_number_per_mmsq': '', 'cd16_plus_cd56_minus_cd134_plus_number_per_mmsq': '', 'cd16_plus_cd56_minus_number_per_mmsq': '', 'cd16_plus_cd56_plus_cd134_plus_number_per_mmsq': '', 'cd16_plus_cd56_plus_number_per_mmsq': '', 'cd16_plus_number_per_mmsq': '', 'cd3_minus_pdl1_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_cd134l_minus_number_per_mmsq': '', 'cd3_plus_cd4_plus_cd134l_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_hladr_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_icos_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_icos_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_ki67_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_pd1_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_pd1_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_hladr_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_icos_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_ki67_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_pd1_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_icos_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_ki67_plus_cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_ki67_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_pdl1_minus_number_per_mmsq': '', 'cd3_plus_cd4_plus_pdl1_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_cd134l_minus_number_per_mmsq': '', 'cd3_plus_cd8_plus_cd134l_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_hladr_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_icos_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_ki67_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_hladr_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_icos_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_ki67_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_icos_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_ki67_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_pdl1_minus_number_per_mmsq': '', 'cd3_plus_cd8_plus_pdl1_plus_number_per_mmsq': '', 'cd3_plus_icos_minus_number_per_mmsq': '', 'cd3_plus_icos_plus_number_per_mmsq': '', 'cd3_plus_number_per_mmsq': '', 'cd3_plus_pdl1_minus_number_per_mmsq': '', 'cd3_plus_pdl1_plus_number_per_mmsq': '', 'cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq': '', 'cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq': '', 'cd4_plus_number_per_mmsq': '', 'cd56_plus_number_per_mmsq': '', 'cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq': '', 'cd8_plus_number_per_mmsq': '', 'foxp3_plus_number_per_mmsq': '', 'granzymeb_plus_number_per_mmsq': '', 'hladr_plus_number_per_mmsq': '', 'icos_plus_cd134_plus_number_per_mmsq': '', 'icos_plus_number_per_mmsq': '', 'ki67_plus_number_per_mmsq': '', 'panck_plus_number_per_mmsq': '', 'pd1_plus_cd134_plus_number_per_mmsq': '', 'pd1_plus_number_per_mmsq': '', 'pdl1_minus_number_per_mmsq': '', 'pdl1_plus_number_per_mmsq': ''
    };

    Object.keys(row).forEach(item =>
      obj[item] = row[item]
    );
    return obj;
  }

  // Get row for nanostring
  getNanostringRow(row) {
    var obj = {
      'PROBE': '', 'STUDY ID': '', 'PHASE': '', 'ARM': '', 'TIMEPOINT': '', 'SUBJECT ID': '', 'SPECIMEN': '', 'RAW_VALUE': '', 'TMM': '', 'MRN': '', 'cyclic_loess': '', 'oracle': '', 'HG': '', 'log_transformation': '', 'CQN': '', 'RLOG': '', 'min_max': '', 'zscore': '', 'STPM': '', 'FPKM': '', 'RPKM': '', 'RC': '', 'UQ': '', 'median': '', 'RSEM': '', 'DEGES': '', 'loggeomeans': '', 'count': '', 'log10': '', 'mean_centering': '', 'mean_standardization': ''
    };

    Object.keys(row).forEach(item =>
      obj[item] = row[item]
    );
    return obj;
  }

  getClinicalRow(row) {
    var obj = {
      'First Name': '', 'Middle Name': '', 'Last Name1': '', 'Last Name2': '', 'Patient ID': '', 'height': '', 'weight': '', 'bmi': '', 'Sex': '', 'Race': '', 'ssn': '', 'dob': '', 'dod': '', 'marital status': '',
      'address1': '', 'address2': '', 'city': '', 'state': '', 'country': '', 'zip_code': '', 'email': '', 'phone': '', 'fax': '', 'photo': '', 'ethnicity': '', 'nationality': '', 'Overall Survival Status': '', 'Study ID': '',
      'PHASE': '', 'New Arm': '', 'Cohort Group': '', 'Enrolling Org': '', 'Enrolling Date': '', 'Concent Date': '', 'client_suid': '', 'GENE Of Intrest1': '', 'GENE Of Intrest1 Mutation ': '',
      'GENE Of Intrest2': '', 'GENE Of Intrest2 mutation': '', 'timepoint': '', 'Primary Tumor/Cancer Type': '', 'disease detail': '', 'Disease Stage at Enrollement': '', 'Diagnosis Age': '',
      'screen detail': '', 'Prior Treatment': '', 'Notes': '', 'Hospital Name': '', 'Date of Admission': '', 'Date of Discharge': '', 'Response': '', 'Response Group': '',
      'Response Duration (months)': '', 'Overall Survival (Months)': '', 'Benefit': '', 'date_of_screening': '', 'screen_failure': '', 'hbv_infection_val_mode': '',
      'hbv_infection_code_mode': '', 'duration_hbv_positive': '', 'PREVIOUSLY_SMOKING': '', 'CURRENTLY_SMOKING': ''
    };
    Object.keys(row).forEach(item =>
      obj[item] = row[item]
    );
    return obj;
  }
  // get radiology row
  getRadiologyRow(row) {
    var obj = {
      'study_id': '', 'phase': '', 'arm': '', 'suid': '', 'timepoint': '', 'assessment_date': '', 'method_of_assessment': '', 'image_name': '', 'image_type': '', 'imagefile': '', 'image_url': '', 'image_thumbnail_url': '', 'block': '', 'identifier': '',
      'slidetype': '', 'description': '', 'time': '', 'size': '', 'magnification': '', 'processed': '', 'result_interpretation': '', 'assay_name': ''
    };
    Object.keys(row).forEach(item =>
      obj[item] = row[item]
    );
    return obj;
  }

  createRow(row, key, splitRowKeys, splitNewKey, mergeUnderHeader = null) {
    var obj = {};
    var obj = JSON.parse(JSON.stringify(row));
    obj[splitNewKey] = obj[key];
    (mergeUnderHeader) ? obj[mergeUnderHeader] = key : null;
    splitRowKeys.forEach(function (omitkey) {
      delete obj[omitkey];
    });
    return obj;
  }

  formatCsvFile(userId, inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, fileType, mergeUnderHeader = null) {
    var mainList = [];
    var fType = fileType;
    var uId = userId;
    var outputHeader = outheader;

    //    
    try {
      var contents = fs.readFileSync(inFile, 'utf8');
      var index = contents.indexOf('\n');
      let tmpLine = [];
      if (index > -1) {
        var line = contents.substring(0, index);
        tmpLine = line.split(',');
      }
      if (tmpLine.length != fileheader.length) {
        var fileName = path.basename(outFile);
        var jobId = fileName.substr(0, fileName.lastIndexOf("."));
        let msg = `Unexpected Error: column header mismatch expected: ${fileheader.length} columns got: ${tmpLine.length}`;
        updateJobStatus(jobId, "Failed", msg);
        return;
      }
    } catch (error) {
      console.log("error while reading file");
    }
    //

    try {
      CsvController.parseCsvFile(inFile, fileheader)
        .then((rows) => {
          let rowNo = 1;
          if (splitRowKeys.length == 0) {
            rows.map(row => {
              rowNo = rowNo + 1;
              if (fileType == 'CYTOKINE') {
                var obj = CsvController.getCytokineRow(row);
                obj.row_no = rowNo;
                mainList.push(obj);
              }
              if (fileType == 'CLINICAL') {
                var obj = CsvController.getClinicalRow(row);
                obj.row_no = rowNo;
                mainList.push(obj);
              }
              if (fileType == 'IFC') {
                var obj = CsvController.getIFCRow(row);
                obj.row_no = rowNo;
                mainList.push(obj);
              }
              if (fileType == 'NANOSTRING') {
                var obj = CsvController.getNanostringRow(row);
                obj.row_no = rowNo;
                mainList.push(obj);
              }
              if (fileType == 'RADIOLOGY') {
                var obj = CsvController.getRadiologyRow(row);
                obj.row_no = rowNo;
                mainList.push(obj);
              }
            });

          } else {
            rows.map(row => {
              rowNo = rowNo + 1;
              for (var i = 0; i < splitRowKeys.length; i++) {
                var obj = CsvController.createRow(row, splitRowKeys[i], splitRowKeys, splitNewKey, mergeUnderHeader);
                obj.row_no = rowNo;
                mainList.push(obj);
              }
            });
          }
        })
        .then(() => {
          console.log('Started output.');
          CsvController.saveCsvFile(outFile, mainList, outheader)
            .then(() => {
              console.log('Finished processing.');
              var fileName = path.basename(outFile);
              var jobId = fileName.substr(0, fileName.lastIndexOf("."));
              //console.log("---" + JSON.stringify(mainList));
              importDataToStage(uId, jobId, mainList, outputHeader, fType);
              //importData(fType, outFile);
            });
        })
        .catch(err => {
          console.error(err.stack);
          var fileName = path.basename(outFile);
          var jobId = fileName.substr(0, fileName.lastIndexOf("."));
          updateJobStatus(jobId, "Failed", err.message);
          //process.exit(1);
        });

    } catch (error) {
      var fileName = path.basename(outFile);
      var jobId = fileName.substr(0, fileName.lastIndexOf("."));
      updateJobStatus(jobId, "Failed", error.message);
    }

  }

  formatCsvFileIFC(userId, inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, fileType, mergeUnderHeader = null) {
    var mainList = [];
    var fType = fileType;
    var uId = userId;
    var outputHeader = outheader;

    //    
    try {
      var contents = fs.readFileSync(inFile, 'utf8');
      var index = contents.indexOf('\n');
      let tmpLine = [];
      if (index > -1) {
        var line = contents.substring(0, index);
        tmpLine = line.split(',');
      }
      if (tmpLine.length != fileheader.length) {
        var fileName = path.basename(outFile);
        var jobId = fileName.substr(0, fileName.lastIndexOf("."));
        let msg = `Unexpected Error: column header mismatch expected: ${fileheader.length} columns got: ${tmpLine.length}`;
        updateJobStatus(jobId, "Failed", msg);
        return;
      }
    } catch (error) {
      console.log("error while reading file");
    }
    //

    try {
      CsvController.parseCsvFile(inFile, fileheader)
        .then((rows) => {
          let rowNo = 1;
          if (splitRowKeys.length == 0) {
            rows.map(row => {
              rowNo = rowNo + 1;
              var obj = getIFCRow(row);
              obj.row_no = rowNo;
              mainList.push(obj);
            });
          } else {
            rows.map(row => {
              rowNo = rowNo + 1;
              for (var i = 0; i < splitRowKeys.length; i++) {
                var obj = CsvController.createRow(row, splitRowKeys[i], splitRowKeys, splitNewKey, mergeUnderHeader);
                obj.measure = 'density';
                obj.row_no = rowNo;
                mainList.push(obj);
              }
            });
          }
        })
        .then(() => {
          console.log('Started output.');
          CsvController.saveCsvFile(outFile, mainList, outheader)
            .then(() => {
              console.log('Finished processing.');
              //importData(fType, outFile);
              var fileName = path.basename(outFile);
              var jobId = fileName.substr(0, fileName.lastIndexOf("."));
              importDataToStage(uId, jobId, mainList, outputHeader, fType);
            })
        })
        .catch(err => {
          console.error(err.stack);
          var fileName = path.basename(outFile);
          var jobId = fileName.substr(0, fileName.lastIndexOf("."));
          updateJobStatus(jobId, "Failed", err.message);
          //process.exit(1);
        });

    } catch (error) {
      var fileName = path.basename(outFile);
      var jobId = fileName.substr(0, fileName.lastIndexOf("."));
      updateJobStatus(jobId, "Failed", error.message);
    }
  }

}();


// Cytokine
const convertFileCytokine = async function (userId, inFile, outFile) {
  var fileheader = ['STUDY ID', undefined, undefined, 'SUBJECT ID', 'MARKER', undefined, 'TIMEPOINT', 'RESULT', undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 'predose_ratio', 'median_ratio', 'cycle_pre_per_post', 'cycle', 'median_op_pg_per_ml_cp_', undefined, undefined, 'median_ratio_response', 'median_pgml_response', undefined, undefined, undefined];
  var outheader = ['STUDY ID', 'PHASE', 'ARM', 'TIMEPOINT', 'SAMPLE BARCODE', 'SUBJECT ID', 'PROCEDURE DATE', 'PANEL', 'MARKER', 'MEASURE', 'RESULT', 'RESULT UNIT', 'POPULATION', 'TMM', 'MRN', 'cyclic_loess', 'oracle', 'HG', 'log_transformation', 'CQN', 'RLOG', 'min_max', 'zscore', 'STPM', 'FPKM', 'RPKM', 'RC', 'UQ', 'median', 'RSEM', 'DEGES', 'loggeomeans', 'count', 'log10', 'mean_centering', 'mean_standardization', 'predose_ratio', 'median_ratio', 'cycle_pre_per_post', 'cycle', 'median_op_pg_per_ml_cp_', 'median_ratio_response', 'median_pgml_response', 'row_no'];
  var splitRowKeys = []; //[] if ignored
  var splitNewKey = ''; //'' if ignored
  try {
    CsvController.formatCsvFile(userId, inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'CYTOKINE');
  } catch (error) {
    console.log(error);
  }
}

// Radiology
const convertFileRadiology = async function (userId, inFile, outFile) {
  var fileheader = ['study_id','phase','arm','suid','timepoint','assessment_date','method_of_assessment','image_name','image_type','imagefile','image_url','image_thumbnail_url','block','identifier','slidetype','description','time','size','magnification','processed','result_interpretation','assay_name'];
  var outheader =  ['study_id','phase','arm','suid','timepoint','assessment_date','method_of_assessment','image_name','image_type','imagefile','image_url','image_thumbnail_url','block','identifier','slidetype','description','time','size','magnification','processed','result_interpretation','assay_name','row_no'];
  var splitRowKeys = []; //[] if ignored
  var splitNewKey = ''; //'' if ignored
  try {
    CsvController.formatCsvFile(userId, inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'RADIOLOGY');
  } catch (error) {
    console.log(error);
  }
}
// Convert IFC
const convertFileIFC = async function (userId, inFile, outFile) {
  //'d_studyid','d_usubjid','d_orig_subjid','batch','sample_type','cell_type','slide','cd134_plus_number_per_mmsq','cd134l_plus_number_per_mmsq','cd16_minus_cd56_plus_cd134_plus_number_per_mmsq','cd16_minus_cd56_plus_number_per_mmsq','cd16_plus_cd56_minus_cd134_plus_number_per_mmsq','cd16_plus_cd56_minus_number_per_mmsq','cd16_plus_cd56_plus_cd134_plus_number_per_mmsq','cd16_plus_cd56_plus_number_per_mmsq','cd16_plus_number_per_mmsq','cd3_minus_pdl1_plus_number_per_mmsq','cd3_plus_cd4_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_cd134l_minus_number_per_mmsq','cd3_plus_cd4_plus_cd134l_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_hladr_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_icos_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_icos_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_ki67_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_pd1_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_pd1_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_hladr_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_icos_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_ki67_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_pd1_plus_number_per_mmsq','cd3_plus_cd4_plus_icos_plus_number_per_mmsq','cd3_plus_cd4_plus_ki67_plus_cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_ki67_plus_number_per_mmsq','cd3_plus_cd4_plus_number_per_mmsq','cd3_plus_cd4_plus_pdl1_minus_number_per_mmsq','cd3_plus_cd4_plus_pdl1_plus_number_per_mmsq','cd3_plus_cd8_plus_cd134_plus_number_per_mmsq','cd3_plus_cd8_plus_cd134l_minus_number_per_mmsq','cd3_plus_cd8_plus_cd134l_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_cd134_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_hladr_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_icos_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_ki67_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_cd134_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_cd134_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_hladr_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_icos_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_ki67_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_number_per_mmsq','cd3_plus_cd8_plus_icos_plus_number_per_mmsq','cd3_plus_cd8_plus_ki67_plus_number_per_mmsq','cd3_plus_cd8_plus_number_per_mmsq','cd3_plus_cd8_plus_pdl1_minus_number_per_mmsq','cd3_plus_cd8_plus_pdl1_plus_number_per_mmsq','cd3_plus_icos_minus_number_per_mmsq','cd3_plus_icos_plus_number_per_mmsq','cd3_plus_number_per_mmsq','cd3_plus_pdl1_minus_number_per_mmsq','cd3_plus_pdl1_plus_number_per_mmsq','cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq','cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq','cd4_plus_number_per_mmsq','cd56_plus_number_per_mmsq','cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq','cd8_plus_number_per_mmsq','foxp3_plus_number_per_mmsq','granzymeb_plus_number_per_mmsq','hladr_plus_number_per_mmsq','icos_plus_cd134_plus_number_per_mmsq','icos_plus_number_per_mmsq','ki67_plus_number_per_mmsq','panck_plus_number_per_mmsq','pd1_plus_cd134_plus_number_per_mmsq','pd1_plus_number_per_mmsq','pdl1_minus_number_per_mmsq','pdl1_plus_number_per_mmsq','acc_number_cont_number','patient_number','pre_post','tissue_type','batch_description','part','cohort_1','cohort_2','dose_mg_kg','dose_op_mg_per_kg_cp_','total_cycles','tumor_type','tumor','tumor_detail','tissue_source','visit_description','visit_date','actual_dose_1','actual_dose_2','actual_dose_3','smart_specimen_class_description','pd_l_1_status_site','pd_op_l_cp_1_status_op_site_cp_','prior_pd_1','prior_pd1_qstn_','prior_ipi','prior_ipi_qstn_','prior_smoking','prior_smoking_qstn_','current_smoker','current_smoker_qstn_','effusion','history_of_effusion','lung_involvement','status2','therapy_type','response','d_subjid','d_cohort','d_visit_name','d_timepoint','timepoint1','pre_post'
  var fileheader = ['study_id', undefined, 'SUBJECT ID', undefined, undefined, 'tissue_category', 'Slide ID', 'cd134_plus_number_per_mmsq', 'cd134l_plus_number_per_mmsq', 'cd16_minus_cd56_plus_cd134_plus_number_per_mmsq', 'cd16_minus_cd56_plus_number_per_mmsq',
    'cd16_plus_cd56_minus_cd134_plus_number_per_mmsq', 'cd16_plus_cd56_minus_number_per_mmsq', 'cd16_plus_cd56_plus_cd134_plus_number_per_mmsq', 'cd16_plus_cd56_plus_number_per_mmsq', 'cd16_plus_number_per_mmsq',
    'cd3_minus_pdl1_plus_number_per_mmsq', 'cd3_plus_cd4_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd4_plus_cd134l_minus_number_per_mmsq', 'cd3_plus_cd4_plus_cd134l_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_cd134_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_minus_hladr_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_minus_icos_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_icos_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_minus_ki67_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_minus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_pd1_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_minus_pd1_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_hladr_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_plus_icos_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_ki67_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_plus_pd1_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_icos_plus_number_per_mmsq', 'cd3_plus_cd4_plus_ki67_plus_cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd4_plus_ki67_plus_number_per_mmsq', 'cd3_plus_cd4_plus_number_per_mmsq', 'cd3_plus_cd4_plus_pdl1_minus_number_per_mmsq', 'cd3_plus_cd4_plus_pdl1_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd8_plus_cd134l_minus_number_per_mmsq', 'cd3_plus_cd8_plus_cd134l_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_minus_cd134_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_minus_hladr_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_icos_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_minus_ki67_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_minus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_hladr_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_plus_icos_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_plus_ki67_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_number_per_mmsq', 'cd3_plus_cd8_plus_icos_plus_number_per_mmsq', 'cd3_plus_cd8_plus_ki67_plus_number_per_mmsq', 'cd3_plus_cd8_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_pdl1_minus_number_per_mmsq', 'cd3_plus_cd8_plus_pdl1_plus_number_per_mmsq', 'cd3_plus_icos_minus_number_per_mmsq', 'cd3_plus_icos_plus_number_per_mmsq', 'cd3_plus_number_per_mmsq',
    'cd3_plus_pdl1_minus_number_per_mmsq', 'cd3_plus_pdl1_plus_number_per_mmsq', 'cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq', 'cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd4_plus_number_per_mmsq', 'cd56_plus_number_per_mmsq', 'cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq', 'cd8_plus_number_per_mmsq', 'foxp3_plus_number_per_mmsq', 'granzymeb_plus_number_per_mmsq',
    'hladr_plus_number_per_mmsq', 'icos_plus_cd134_plus_number_per_mmsq', 'icos_plus_number_per_mmsq', 'ki67_plus_number_per_mmsq', 'panck_plus_number_per_mmsq', 'pd1_plus_cd134_plus_number_per_mmsq', 'pd1_plus_number_per_mmsq',
    'pdl1_minus_number_per_mmsq', 'pdl1_plus_number_per_mmsq', undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 'timepoint', undefined];

  //'study_id','Phase','ARM', 'timepoint', 'SUBJECT ID', 'SPECIMEN', 'Slide  Label ', 'Slide ID', 'tissue_category', 'tissue_area', 'marker', 'measure', 'grade', 'result', 'result_unit', 'TMM', 'MRN', 'cyclic_loess', 'oracle', 'HG', 'log_transformation', 'CQN', 'RLOG', 'min_max', 'zscore', 'STPM', 'FPKM', 'RPKM', 'RC', 'UQ', 'median', 'RSEM', 'DEGES', 'loggeomeans', 'count', 'log10', 'mean_centering', 'mean_standardisation'
  var outheader = ['study_id', 'Phase', 'ARM', 'timepoint', 'SUBJECT ID', 'SPECIMEN', 'Slide  Label ', 'Slide ID', 'tissue_category', 'tissue_area', 'marker', 'measure', 'grade', 'result', 'result_unit', 'TMM', 'MRN', 'cyclic_loess', 'oracle', 'HG', 'log_transformation', 'CQN', 'RLOG', 'min_max', 'zscore', 'STPM', 'FPKM', 'RPKM', 'RC', 'UQ', 'median', 'RSEM', 'DEGES', 'loggeomeans', 'count', 'log10', 'mean_centering', 'mean_standardisation', 'row_no'];

  var splitRowKeys = [
    'cd134_plus_number_per_mmsq',
    'cd134l_plus_number_per_mmsq',
    'cd16_minus_cd56_plus_cd134_plus_number_per_mmsq',
    'cd16_minus_cd56_plus_number_per_mmsq',
    'cd16_plus_cd56_minus_cd134_plus_number_per_mmsq',
    'cd16_plus_cd56_minus_number_per_mmsq',
    'cd16_plus_cd56_plus_cd134_plus_number_per_mmsq',
    'cd16_plus_cd56_plus_number_per_mmsq',
    'cd16_plus_number_per_mmsq',
    'cd3_minus_pdl1_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_cd134l_minus_number_per_mmsq',
    'cd3_plus_cd4_plus_cd134l_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_hladr_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_icos_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_icos_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_ki67_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_pd1_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_hladr_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_icos_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_ki67_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_pd1_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_icos_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_ki67_plus_cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_ki67_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_pdl1_minus_number_per_mmsq',
    'cd3_plus_cd4_plus_pdl1_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_cd134l_minus_number_per_mmsq',
    'cd3_plus_cd8_plus_cd134l_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_hladr_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_icos_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_ki67_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_hladr_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_icos_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_ki67_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_icos_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_ki67_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_pdl1_minus_number_per_mmsq',
    'cd3_plus_cd8_plus_pdl1_plus_number_per_mmsq',
    'cd3_plus_icos_minus_number_per_mmsq',
    'cd3_plus_icos_plus_number_per_mmsq',
    'cd3_plus_number_per_mmsq',
    'cd3_plus_pdl1_minus_number_per_mmsq',
    'cd3_plus_pdl1_plus_number_per_mmsq',
    'cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq',
    'cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd4_plus_number_per_mmsq',
    'cd56_plus_number_per_mmsq',
    'cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd8_plus_number_per_mmsq',
    'foxp3_plus_number_per_mmsq',
    'granzymeb_plus_number_per_mmsq',
    'hladr_plus_number_per_mmsq',
    'icos_plus_cd134_plus_number_per_mmsq',
    'icos_plus_number_per_mmsq',
    'ki67_plus_number_per_mmsq',
    'panck_plus_number_per_mmsq',
    'pd1_plus_cd134_plus_number_per_mmsq',
    'pd1_plus_number_per_mmsq',
    'pdl1_minus_number_per_mmsq',
    'pdl1_plus_number_per_mmsq'
  ]; //[] if ignored
  var splitNewKey = 'result'; //'' if ignored
  CsvController.formatCsvFileIFC(userId, inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'ifc', 'marker');
}

// Convert Nanostring file
const convertFileNanostring = async function (userId, inFile, outFile) {
  //'study_id','d_studyid','subject_number','d_orig_subjid','d_usubjid','specimen_id','visit','lims_id','sample_id','gene_name','class_name','standard_result_numeric','d_subjid','d_visit_name','d_timepoint'
  var fileheader = [undefined, 'STUDY ID', undefined, 'SUBJECT ID', undefined, undefined, undefined, undefined, 'SPECIMEN', 'PROBE', undefined, 'RAW_VALUE', undefined, 'TIMEPOINT', undefined];
  //'PROBE','STUDY ID','PHASE','ARM','TIMEPOINT','SUBJECT ID','SPECIMEN','RAW_VALUE','TMM','MRN','cyclic_loess','oracle','HG','log_transformation','CQN','RLOG','min_max','zscore','STPM','FPKM','RPKM','RC','UQ','median','RSEM','DEGES','loggeomeans','count','log10','mean_centering','mean_standardization'
  var outheader = ['PROBE', 'STUDY ID', 'PHASE', 'ARM', 'TIMEPOINT', 'SUBJECT ID', 'SPECIMEN', 'RAW_VALUE', 'TMM', 'MRN', 'cyclic_loess', 'oracle', 'HG', 'log_transformation', 'CQN', 'RLOG', 'min_max', 'zscore', 'STPM', 'FPKM', 'RPKM', 'RC', 'UQ', 'median', 'RSEM', 'DEGES', 'loggeomeans', 'count', 'log10', 'mean_centering', 'mean_standardization', 'row_no'];
  var splitRowKeys = []; //[] if ignored
  var splitNewKey = ''; //'' if ignored
  CsvController.formatCsvFile(userId, inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'NANOSTRING');
}

// ihc
const convertFileIhc = async function (userId, inFile, outFile) {
  var fileheader = ['STUDY ID', undefined, undefined, undefined, 'TIMEPOINT', 'SUBJECT ID', 'SAMPLE BARCODE', 'CLIENT SAMPLE BARCODE', undefined, 'MARKER',
    undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 'combined_positive_score',
    'tumor_fractional_intensity_cln_prct_0', 'tumor_fractional_intensity_cln_prct_1_pls_', 'tumor_fractional_intensity_cln_prct_2_pls_',
    'tumor_fractional_intensity_cln_prct_3_pls_', 'tumor_proportion_score', 'aretaicspresent', 'density_op_mids_cp_', 'mean_intensity_op_mimi_cp_',
    undefined, undefined, undefined];

  var outheader = ['STUDY ID', 'PHASE', 'ARM', 'TIMEPOINT', 'SAMPLE BARCODE', 'CLIENT SAMPLE BARCODE', 'SUBJECT ID', 'PROCEDURE DATE', 'MARKER', 'MEASURE',
    'TISSUE', 'tissue_area', 'grade', 'region', 'RESULT', 'RESULT UNIT', 'TMM', 'MRN', 'cyclic_loess', 'oracle', 'HG', 'log_transformation', 'CQN', 'RLOG', 'min_max',
    'zscore', 'count', 'RC', 'UQ', 'median', 'RSEM', 'STPM', 'FPKM', 'RPKM', 'DEGES', 'loggeomeans', 'log10', 'mean_centering', 'mean_standardization', 'row_no'];

  var splitRowKeys = ['combined_positive_score', 'tumor_fractional_intensity_cln_prct_0', 'tumor_fractional_intensity_cln_prct_1_pls_',
    'tumor_fractional_intensity_cln_prct_2_pls_', 'tumor_fractional_intensity_cln_prct_3_pls_', 'tumor_proportion_score', 'aretaicspresent',
    'density_op_mids_cp_', 'mean_intensity_op_mimi_cp_']; //[] if ignored

  var splitNewKey = 'RESULT';
  CsvController.formatCsvFile(userId, inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'IHC', 'MEASURE');
}

// cytometry
const convertFileCytometry = async function (userId, inFile, outFile) {
  var fileheader = ['STUDY ID', undefined, undefined, 'SUBJECT ID', undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    'PANEL', 'MARKER', undefined, 'iso_value', 'iso_units', undefined, 'si_value', 'si_units', undefined, 'other_value', 'other_units',
    undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    'TIMEPOINT', 'd_timepoint'];

  var outheader = ['STUDY ID', 'PHASE', 'ARM', 'TIMEPOINT', 'SAMPLE BARCODE', 'SUBJECT ID', 'PROCEDURE DATE', 'PANEL', 'MARKER', 'MEASURE', 'RESULT', 'RESULT UNIT',
    'Parent Population', 'POPULATION', 'TMM', 'MRN', 'cyclic_loess', 'oracle', 'HG', 'log_transformation', 'CQN', 'RLOG', 'min_max', 'zscore', 'COUNT', 'RC', 'UQ', 'MEDIAN',
    'RSEM', 'STPM', 'FPKM', 'RPKM', 'DEGES', 'LOGGEOMEANS', 'LOG10', 'MEAN_CENTERING', 'MEAN_STANDARDIZATION', 'row_no'];

  var splitRowKeys = ['iso_value', 'si_value', 'other_value']; //[] if ignored
  var splitNewKey = 'RESULT';
  CsvController.formatCsvFile(userId, inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'FLOW CYTOMETRY', 'MEASURE');
}

// clinical
const convertFileClinical = async function (userId, inFile, outFile) {
  var fileheader = ['Study ID', undefined, 'Patient ID', 'New Arm', 'Cohort Group', 'Response', undefined, undefined, undefined, 'PREVIOUSLY_SMOKING', 'CURRENTLY_SMOKING', undefined,
    'Primary Tumor/Cancer Type', 'disease detail', 'Disease Stage at Enrollement', undefined, undefined, undefined, undefined, undefined, undefined, undefined];

  var outheader = ['First Name', 'Middle Name', 'Last Name1', 'Last Name2', 'Patient ID', 'height', 'weight', 'bmi', 'Sex', 'Race', 'ssn', 'dob', 'dod',
    'marital status', 'address1', 'address2', 'city', 'state', 'country', 'zip_code', 'email', 'phone', 'fax', 'photo', 'ethnicity', 'nationality',
    'Overall Survival Status', 'Study ID', 'PHASE', 'New Arm', 'Cohort Group', 'Enrolling Org', 'Enrolling Date', 'Concent Date', 'client_suid', 'GENE Of Intrest1',
    'GENE Of Intrest1 Mutation ', 'GENE Of Intrest2', 'GENE Of Intrest2 mutation', 'timepoint', 'Primary Tumor/Cancer Type', 'disease detail',
    'Disease Stage at Enrollement', 'Diagnosis Age', 'screen detail', 'Prior Treatment', 'Notes', 'Hospital Name', 'Date of Admission', 'Date of Discharge',
    'Response', 'Response Group', 'Response Duration (months)', 'Overall Survival (Months)', 'Benefit', 'date_of_screening', 'screen_failure',
    'hbv_infection_val_mode', 'hbv_infection_code_mode', 'duration_hbv_positive', 'PREVIOUSLY_SMOKING', 'CURRENTLY_SMOKING', 'row_no'];

  var splitRowKeys = []; //[] if ignored
  var splitNewKey = '';
  CsvController.formatCsvFile(userId, inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'CLINICAL');
}

//DB functions
// const importData = async function (fileType, outFile) {
//   var fileName = path.basename(outFile);
//   var jobId = fileName.substr(0, fileName.lastIndexOf("."));

//   const client = new Client({
//     connectionString: connectionString,
//   })
//   //client.connect();

//   client.connect(err => {
//     if (err) {
//       console.error('DB connection error', err.stack);
//     } else {
//       console.log('DB connected');
//     }
//   })

//   if (fileType.toUpperCase() == "CLINICAL") {
//     fileType = "SUBJECT";
//   }

//   console.log("Filetype " + fileType);
//   console.log("outFile " + outFile);
//   console.log("jobId " + jobId);

//   let sql = `select import_data('${fileType}','${outFile}','${jobId}')`;
//   client.query(sql, (err, res) => {
//     if (err) {
//       console.log(err);
//       updateJobStatus(jobId, "Failed", err.message);
//     } else {
//       let msg = '';
//       if (res && res.rows && (res.rows.length > 0) && res.rows[0].import_data) {
//         let skippedRec = res.rows[0].import_data["Records Skipped"];
//         if (skippedRec > 0) {
//           msg = "Records Skipped: " + res.rows[0].import_data["Records Skipped"];
//           updateJobStatus(jobId, "Partial Success", msg);
//         } else {
//           updateJobStatus(jobId, "Completed", '');
//         }
//       } else {
//         console.log("Error---" + JSON.stringify(res.rows[0]));
//         updateJobStatus(jobId, "Failed", 'Import data failed');
//       }
//     }

//     client.end();

//   });

// }


const importDataToStage = async function (userId, jobId, mainList, outheader, fileType) {

  try {

    const client = new Client({
      connectionString: connectionString,
    })

    await client.connect();
    console.log('DB connected');

    let stageFnName = "";
    let importDataFnName = "";
    if (fileType.toUpperCase() == "IHC") {
      stageFnName = "import_ihc_records_to_stage";
      importDataFnName = "import_ihc";
    }
    if (fileType.toUpperCase() == "IFC") {
      stageFnName = "import_ifc_records_to_stage"; // check with swetal
      importDataFnName = "import_ifc";
    }
    if (fileType.toUpperCase() == "CYTOKINE") {
      stageFnName = "import_cytokine_records_to_stage"; // check with swetal
      importDataFnName = "import_cytokine";
    }
    if (fileType.toUpperCase() == "CLINICAL") {
      stageFnName = "import_subject_records_to_stage"; // check with swetal
      importDataFnName = "import_subject";
    }
    if (fileType.toUpperCase() == "FLOW CYTOMETRY") {
      stageFnName = "import_flow_cytometry_records_to_stage";// check with swetal
      importDataFnName = "import_flow_cytometry";
    }
    if (fileType.toUpperCase() == "NANOSTRING") {
      stageFnName = "import_nanostring_records_to_stage";
      importDataFnName = "import_nanostring";
    }

    if (fileType.toUpperCase() == "RADIOLOGY") {
      stageFnName = "import_image_records_to_stage"; // check with swetal
      importDataFnName = "import_radiology";
    }
    // for each row
    for (let item of mainList) {
      let sql = '';
      sql = `select ${stageFnName} ('${jobId}' , ${item['row_no']} `;
      outheader.forEach(key => {
        if (key != 'row_no') {
          let tmpVal = item[key] ? item[key].trim() : undefined;
          if (tmpVal == undefined) {
            sql = sql + `,NULL`;
          } else {
            sql = sql + `,'${tmpVal}'`;
          }
        }
      });
      sql = sql + ')';
      //console.log(sql);
      try {
        let res = await client.query(sql);
      } catch (err) {
        console.log("Error while importing records into stage ");
        console.log("Error" + err);
      }
    }

    // import data
    try {
      let sql = `select ${importDataFnName}('${jobId}' , '${userId}' )`;
      let res = await client.query(sql);
      let msg = '';
      if (res && res.rows && (res.rows.length > 0) && res.rows[0][importDataFnName]) {
        let skippedRec = res.rows[0][importDataFnName]["RECORDS SKIPPED"] ? res.rows[0][importDataFnName]["RECORDS SKIPPED"] : 0;
        if (skippedRec > 0) {
          msg = "Records to be imported: " + (res.rows[0][importDataFnName]["RECORDS TO BE IMPORTED"] ? res.rows[0][importDataFnName]["RECORDS TO BE IMPORTED"] : 0);
          msg += "<BR>Records Imported      : " + (res.rows[0][importDataFnName]["RECORDS IMPORTED"] ? res.rows[0][importDataFnName]["RECORDS IMPORTED"] : 0);
          msg += "<BR>Records Skipped       : " + (res.rows[0][importDataFnName]["RECORDS SKIPPED"] ? res.rows[0][importDataFnName]["RECORDS SKIPPED"] : 0);
          let tmpCnt = res.rows[0][importDataFnName]["RECORDS IMPORTED"] ? res.rows[0][importDataFnName]["RECORDS IMPORTED"] : 0;
          if (tmpCnt == 0) {
            updateJobStatus(jobId, "Failed", msg);
          } else {
            updateJobStatus(jobId, "Partial Success", msg);
          }

        } else {
          msg = "Records to be imported: " + (res.rows[0][importDataFnName]["RECORDS TO BE IMPORTED"] ? res.rows[0][importDataFnName]["RECORDS TO BE IMPORTED"] : 0);
          msg += "<BR>Records Imported      : " + (res.rows[0][importDataFnName]["RECORDS IMPORTED"] ? res.rows[0][importDataFnName]["RECORDS IMPORTED"] : 0);
          msg += "<BR>Records Skipped      : " + (res.rows[0][importDataFnName]["RECORDS SKIPPED"] ? res.rows[0][importDataFnName]["RECORDS SKIPPED"] : 0);
          updateJobStatus(jobId, "Completed", msg);
        }
      } else {
        console.log("Error---" + JSON.stringify(res.rows[0]));
        updateJobStatus(jobId, "Failed", 'Import data failed');
      }
      await clearCache();
    } catch (error) {
      console.log("Error while importing data");
      console.log("Error" + error);
      updateJobStatus(jobId, "Failed", error.message);
    }

    client.end();
    console.log('DB connection closed');
    

  } catch (error) {
    console.log("Error. Please check DB connection");
    updateJobStatus(jobId, "Failed", error.message);
  }

}

const getLogByJobId = async function (req, res) {

  let finalObject = {
    'log': '',
    'status': 200
  };

  try {
    let jobId = req.query.jobId;
    let fileType = req.query.fileType;

    const client = new Client({
      connectionString: connectionString,
    })

    await client.connect();
    console.log('DB connected');

    let badTable = "";
    if (fileType.toUpperCase() == "IHC") {
      badTable = "ihc_bad";
    }
    if (fileType.toUpperCase() == "IFC") {
      badTable = "ifc_bad";
    }
    if (fileType.toUpperCase() == "CYTOKINE") {
      badTable = "cytokine_bad";
    }
    if (fileType.toUpperCase() == "CLINICAL") {
      badTable = "subject_bad";
    }
    if (fileType.toUpperCase() == "FLOW CYTOMETRY") {
      badTable = "flow_cytometry_bad";
    }
    if (fileType.toUpperCase() == "NANOSTRING") {
      badTable = "nanostring_bad";
    }
    if (fileType.toUpperCase() == "RADIOLOGY") {
      badTable = "radiology_bad";
    }
    
    let sql = `select distinct csv_row_number as rownum, error_message as errormessage from ${badTable} where job_id = '${jobId}' order by csv_row_number asc`;
    try {
      let result = await client.query(sql);
      if (result && result.rows) {
        finalObject.log = result.rows;
      }
      //console.log("result" + JSON.stringify(result));
      return res.json(finalObject);
    } catch (err) {
      console.log("Error" + err);
      return res.json(finalObject);
    }

    client.end();
    console.log('DB connection closed');

  } catch (error) {
    console.log("Error" + error);
    return res.json(finalObject);
  }
}

const getUser = async function (req, res) {

  let finalObject = {
    'userName': '',
    'status': 200
  };

  try {
    let user = req.query.userName;
    finalObject.userName = user;
    return res.status(200).json(finalObject);
  } catch (error) {
    console.log("Error in getUser()");
    return res.status(401).json(finalObject);
  }
}

const deleteData = async function (req, res) {

  let finalObject = {
    'msg': 'Cleanup process completed successfully.',
    'status': 200
  };

  try {
    let studyId = req.body.reqParams.studyId;
    let fileType = req.body.reqParams.fileType;
    let userName = req.body.reqParams.userName;
    const client = new Client({
      connectionString: connectionString,
    })
    await client.connect();
    console.log('DB connected');
    let sql = `select delete_data_for_study_file_type(${studyId},'${fileType}','${userName}' )`;
    console.log('SQL: ' + sql);
    let resultResponse = await client.query(sql);
    client.end();
    console.log('DB connection closed');
    await clearCache();   
    return res.status(200).json(finalObject);
  } catch (error) {
    console.log("Error while deleting data: " + error);
    finalObject.msg = "Error while deleting data";
    finalObject.status = 404;
    return res.status(200).json(finalObject);
  }
}

const generateFCSConfig = async function (req, res) {
  let finalObject = {
    'msg': 'FCS configuration generated successfully.',
    'status': 200
  };
  try {
    let inputFilePath = req.body.reqParams.inputFilePath;
    let fcsConfigFolder = config.FCS_CONFIG_FOLDER_PATH;
    let fcsConfigFileName = config.FCS_CONFIG_FILE_NAME;
    let fcsDataSourceFolder = config.FCS_DATA_SOURCE_FOLDER;
    let fcsConfigFileFullpath = fcsConfigFolder + '/' + fcsConfigFileName;
    if (!fs.existsSync(fcsConfigFileFullpath)){
      finalObject.status = 404;
      finalObject.msg = 'FCS config file not found';
    }else{
      let xmlData = fs.readFileSync(fcsConfigFileFullpath, {encoding:'utf8'}); 
      let start = xmlData.indexOf("<local_file>");
      let end = xmlData.indexOf("</local_file>");
      if(start > -1 && end > -1 ){
        let filename = path.basename(inputFilePath);
        let substitute = `<local_file>${fcsDataSourceFolder}${filename}`;
        xmlData = xmlData.substring(0, start) + substitute + xmlData.substring(end);
        fs.writeFileSync(fcsConfigFileFullpath, xmlData); 
        console.log("FCS Config file written successfully"); 
      }else{
        finalObject.msg = "Check config file for local_file tag";
        finalObject.status = 404;
        console.log("Check config file for local_file tag"); 
      }
    }
    return res.status(200).json(finalObject);
  } catch (error) {
    console.log("Error while creating FCS config file: " + error);
    finalObject.msg = "Error while creating FCS config file";
    finalObject.status = 404;
    return res.status(200).json(finalObject);
  }
}

var jobController = {
  'createJob': createJob,
  'getJob': getJob,
  'updateJobStatus': updateJobStatus,
  'getJobs': getJobs,
  'getLogByJobId': getLogByJobId,
  'getUser': getUser,
  'deleteData': deleteData,
  'generateFCSConfig': generateFCSConfig
};

module.exports = jobController;
