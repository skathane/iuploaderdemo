FROM node:10.15.3
LABEL maintainer="avinash.gupta@indxtechnology.com"

ARG BUILD_DATE
ARG VCSREF=0
ARG BUILD_VERSION=4.0.1

ENV VCSREF=$VCSREF

# Labels.
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name="icorev4/platform-api-uploader"
LABEL org.label-schema.description="iCorev4 Platform API served from binary"
LABEL org.label-schema.url="http://indxtechnology.com/"
LABEL org.label-schema.vcs-url="https://bitbucket.org/indxtechnology/icorev4-uploader"
LABEL org.label-schema.vcs-ref=$VCSREF
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.docker.cmd="docker run -d -p 6000:6000 -p 6001:6001 --env-file=.env-local --name icore-uploader gcr.io/thermal-shuttle-153100/icore-v4-uploader:dev"


WORKDIR /usr/src/app
COPY package*.json ./

SHELL ["/bin/bash", "-c"]

RUN npm install

COPY . .

# RUN pkg -t node10.15.3 .
# RUN set -o pipefail && ls | grep -v 'icorev4\|logs\|config\|ssh\|ssl\|node_modules\|views\|api-docs\|api\|entrypoint.sh' | xargs rm -rf
# RUN set -o pipefail && cd api/v1 && ls | grep -v 'specs' | xargs rm -rf
EXPOSE 6000 6001

# ENTRYPOINT [ "entrypoint.sh" ]
# CMD ["/usr/src/app/icorev4"]
CMD ["npm", "start"]