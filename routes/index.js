var express = require('express');
var router = express.Router();
var config = require('config');
var fs = require('fs');

const dbConfig = config.get('DB_CONFIG');
const {
  Pool
} = require('pg');
const pool = new Pool({
  user: dbConfig.USERNAME,
  host: dbConfig.HOSTNAME,
  database: dbConfig.DATABASE,
  password: dbConfig.PASSWORD,
  port: dbConfig.PORT
});

// pool.query('SELECT * from accession', (err, res) => {
//   //console.log(err, res);
//   pool.end();
// })

var JobController = require(`../controllers/JobController`);
const uniqid = require('uniqid');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

var multer = require('multer');

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
  let ext = file.originalname.split('.').pop();
    if(ext.toLocaleLowerCase() == 'csv'){
      var inputCSVPath = config.CSV_FILE_INPUT_PATH;
      if (!fs.existsSync(inputCSVPath)){
        fs.mkdirSync(inputCSVPath);
      }
      cb(null, inputCSVPath);
    }else if(ext.toLocaleLowerCase() == 'fcs'){
      var inputFCSPath = config.FCS_FILE_INPUT_PATH;
      if (!fs.existsSync(inputFCSPath)){
        fs.mkdirSync(inputFCSPath);
      }
      cb(null, inputFCSPath);
    }else{
      var inputOtherPath = config.OTHER_FILE_INPUT_PATH;
      if (!fs.existsSync(inputOtherPath)){
        fs.mkdirSync(inputOtherPath);
      }
      cb(null, inputOtherPath);
    }
  },
  filename: (req, file, cb) => {
    
    // var filetype = 'csv';
    // if (file.mimetype === 'text/csv') {
    //   filetype = 'csv';
    // } else {
    //   return next(new Error('INVALID_FILE_TYPE'));
    // }
    var originalname = file.originalname;
    var fileName = originalname.substr(0, originalname.lastIndexOf("."));
    var filetype = originalname.split('.').pop(); 
    cb(null, fileName + '_' + Date.now() + '.' + filetype);
  }
});

var upload = multer({
  storage: storage
});

var getApiInstance = require('./apiInstance');

let tokenCheck = async (req, res, next) => {
  try {
    let token = req.headers['x-access-token'] || req.headers['authorization'] || req.headers['access-token'];
    let apiInstance = getApiInstance(token);
    let response = await apiInstance.get('tokenCheck/' ); 
    if(response.status && response.status == 200){
      next();    
    }else{
      console.log("Authentication Error :: Invalid Token");
      return res.status(401).json({"code": "INDX006","description": "Authentication Error :: Invalid Token"});
    }
  } catch (error) {
    //console.log(error);
    console.log("Authentication Error :: Invalid Token");
    return res.status(401).json({"code": "INDX006","description": "Authentication Error :: Invalid Token"});
  }

}

router.post('/upload', tokenCheck, upload.single('file'), function (req, res, next) {
  //console.log(req.file);
  //console.log(req.body.fileType);
  if (!req.file) {
    res.status(500);
    return next(err);
  }

  var job = {
    jobId: '',
    userId: '',
    originalFileName: '',
    inputFilePath: '',
    fileType: '',
    uploadDate: '',
    errorDetail: '',
    status: '',
    reason: ''
  };

  job.jobId =  uniqid();  //req.file.filename;
  job.userId = req.body.userId;//'admin@indxtechnology.com';
  job.originalFileName = req.file.originalname;
  job.inputFilePath = req.file.path;
  job.fileType = req.body.fileType;
  job.reason = req.body.reason;
  job.uploadDate = new Date();
  job.startTime =  Date.now();
  job.errorDetail = '';
  job.status = "Uploaded";

  JobController.createJob(job);
  res.json({ msg: 'File is uploaded successfully', statusCode: 200 });
});

router.get('/jobs', tokenCheck, JobController.getJobs);
router.get('/logs', tokenCheck, JobController.getLogByJobId);
router.get('/userDetails', tokenCheck, JobController.getUser);
router.post('/deleteData',tokenCheck, JobController.deleteData);

router.post('/generateFCSConfig',tokenCheck, JobController.generateFCSConfig);


module.exports = router;
