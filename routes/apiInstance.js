const axios = require('axios');
const config = require('config');
var apiInstance;
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

module.exports = function getApiInstance (token) {
  apiInstance = axios.create({
    baseURL: config.get('API_URL') + '/api/v1',
    headers: {
      'Authorization': token,
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  });
  return apiInstance;
};
