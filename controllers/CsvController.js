const fs = require('fs');
const csv = require('fast-csv');
const config = require('config');
var DBController = require(`../controllers/DBController`);

const CsvController = new class {
  constructor() {
  }
  parseCsvFile(filePath, inheader) {
    return new Promise((resolve, reserr) => {
      let returnList = [];
      const stream = fs.createReadStream(filePath);
      stream.on('error', function (err) {
        reserr(err)
      });
      csv
        .parseStream(stream, {
          headers: true,
          headers: inheader,
          renameHeaders: true,
          ignoreEmpty: true
        })
        .on('error', (error) => {
          console.error(error)
          reserr(error)
        })
        .on('data', (row) => {
          returnList.push(row);
        })
        .on('end', (rowCount) => {
          console.log(`Parsed ${rowCount} rows`);
          resolve(returnList);
        })
    });
  }

  saveCsvFile(filePath, rows, outorder) {
    return new Promise((resolve, reserr) => {
      let returnList = [];
      const stream = fs.createWriteStream(filePath);
      stream.on('error', function (err) {
        reserr(err);
      });

      csv
        .writeToStream(stream, rows, {
          headers: true,
          headers: outorder
        })
        .on('error', (error) => {
          console.error(error);
          reserr(error);
        })
        .on('finish', () => {
          console.log('Done writing.');
          resolve();
        })
    });
  }
  // Get cytokine row
  getCytokineRow(row) {
    var obj = {
      'STUDY ID': '', 'PHASE': '', 'ARM': '', 'TIMEPOINT': '', 'SAMPLE BARCODE': '', 'SUBJECT ID': '', 'PROCEDURE DATE': '', 'PANEL': '', 'MARKER': '', 'MEASURE': '', 'RESULT': '', 'RESULT UNIT': '', 'POPULATION': '', 'TMM': '', 'MRN': '', 'cyclic_loess': '', 'oracle': '', 'HG': '', 'log_transformation': '', 'CQN': '', 'RLOG': '', 'min_max': '', 'zscore': '', 'STPM': '', 'FPKM': '', 'RPKM': '', 'RC': '', 'UQ': '', 'median': '', 'RSEM': '', 'DEGES': '', 'loggeomeans': '', 'count': '', 'log10': '', 'mean_centering': '', 'mean_standardization': '', 'predose_ratio': '', 'median_ratio': '', 'cycle_pre_per_post': '', 'cycle': '', 'median_op_pg_per_ml_cp_': '', 'median_ratio_response': '', 'median_pgml_response': ''
    };
    Object.keys(row).forEach(item =>
      obj[item] = row[item]
    );
    return obj;
  }

  // Get IFC row
  getIFCRow(row) {
    var obj = {
      'STUDY ID': '', 'PHASE': '', 'ARM': '', 'TIMEPOINT': '', 'SUBJECT ID': '', 'SPECIMEN': '', 'Slide  Label ': '', 'Slide ID': '', 'Tissue Category': '', 'TISSUE AREA (mm2)': '', 'MARKER ': '', 'MEASURE': '', 'GRADE': '', 'RESULT': '', 'RESULT UNIT': '', 'TMM': '', 'MRN': '', 'cyclic_loess': '', 'oracle': '', 'HG': '', 'log_transformation': '', 'CQN': '', 'RLOG': '', 'min_max': '', 'zscore': '', 'STPM': '', 'FPKM': '', 'RPKM': '', 'RC': '', 'UQ': '', 'median': '', 'RSEM': '', 'DEGES': '', 'loggeomeans': '', 'count': '', 'log10': '', 'mean_centering': '', 'mean_standardization': '', 'cd134_plus_number_per_mmsq': '', 'cd134l_plus_number_per_mmsq': '', 'cd16_minus_cd56_plus_cd134_plus_number_per_mmsq': '', 'cd16_minus_cd56_plus_number_per_mmsq': '', 'cd16_plus_cd56_minus_cd134_plus_number_per_mmsq': '', 'cd16_plus_cd56_minus_number_per_mmsq': '', 'cd16_plus_cd56_plus_cd134_plus_number_per_mmsq': '', 'cd16_plus_cd56_plus_number_per_mmsq': '', 'cd16_plus_number_per_mmsq': '', 'cd3_minus_pdl1_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_cd134l_minus_number_per_mmsq': '', 'cd3_plus_cd4_plus_cd134l_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_hladr_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_icos_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_icos_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_ki67_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_pd1_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_minus_pd1_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_hladr_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_icos_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_ki67_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_foxp3_plus_pd1_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_icos_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_ki67_plus_cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_ki67_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_number_per_mmsq': '', 'cd3_plus_cd4_plus_pdl1_minus_number_per_mmsq': '', 'cd3_plus_cd4_plus_pdl1_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_cd134l_minus_number_per_mmsq': '', 'cd3_plus_cd8_plus_cd134l_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_hladr_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_icos_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_ki67_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_hladr_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_icos_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_ki67_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_icos_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_ki67_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_number_per_mmsq': '', 'cd3_plus_cd8_plus_pdl1_minus_number_per_mmsq': '', 'cd3_plus_cd8_plus_pdl1_plus_number_per_mmsq': '', 'cd3_plus_icos_minus_number_per_mmsq': '', 'cd3_plus_icos_plus_number_per_mmsq': '', 'cd3_plus_number_per_mmsq': '', 'cd3_plus_pdl1_minus_number_per_mmsq': '', 'cd3_plus_pdl1_plus_number_per_mmsq': '', 'cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq': '', 'cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq': '', 'cd4_plus_number_per_mmsq': '', 'cd56_plus_number_per_mmsq': '', 'cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq': '', 'cd8_plus_number_per_mmsq': '', 'foxp3_plus_number_per_mmsq': '', 'granzymeb_plus_number_per_mmsq': '', 'hladr_plus_number_per_mmsq': '', 'icos_plus_cd134_plus_number_per_mmsq': '', 'icos_plus_number_per_mmsq': '', 'ki67_plus_number_per_mmsq': '', 'panck_plus_number_per_mmsq': '', 'pd1_plus_cd134_plus_number_per_mmsq': '', 'pd1_plus_number_per_mmsq': '', 'pdl1_minus_number_per_mmsq': '', 'pdl1_plus_number_per_mmsq': ''
    };

    Object.keys(row).forEach(item =>
      obj[item] = row[item]
    );
    return obj;
  }

  // Get row for nanostring
  getNanostringRow(row) {
    var obj = {
      'PROBE': '', 'STUDY ID': '', 'PHASE': '', 'ARM': '', 'TIMEPOINT': '', 'SUBJECT ID': '', 'SPECIMEN': '', 'RAW_VALUE': '', 'TMM': '', 'MRN': '', 'cyclic_loess': '', 'oracle': '', 'HG': '', 'log_transformation': '', 'CQN': '', 'RLOG': '', 'min_max': '', 'zscore': '', 'STPM': '', 'FPKM': '', 'RPKM': '', 'RC': '', 'UQ': '', 'median': '', 'RSEM': '', 'DEGES': '', 'loggeomeans': '', 'count': '', 'log10': '', 'mean_centering': '', 'mean_standardization': ''
    };

    Object.keys(row).forEach(item =>
      obj[item] = row[item]
    );
    return obj;
  }

  getClinicalRow(row) {
    var obj = {
      'First Name': '', 'Middle Name': '', 'Last Name1': '', 'Last Name2': '', 'Patient ID': '', 'height': '', 'weight': '', 'bmi': '', 'Sex': '', 'Race': '', 'ssn': '', 'dob': '', 'dod': '', 'marital status': '',
      'address1': '', 'address2': '', 'city': '', 'state': '', 'country': '', 'zip_code': '', 'email': '', 'phone': '', 'fax': '', 'photo': '', 'ethnicity': '', 'nationality': '', 'Overall Survival Status': '', 'Study ID': '',
      'PHASE': '', 'New Arm': '', 'Cohort Group': '', 'Enrolling Org': '', 'Enrolling Date': '', 'Concent Date': '', 'client_suid': '', 'GENE Of Intrest1': '', 'GENE Of Intrest1 Mutation ': '',
      'GENE Of Intrest2': '', 'GENE Of Intrest2 mutation': '', 'timepoint': '', 'Primary Tumor/Cancer Type': '', 'disease detail': '', 'Disease Stage at Enrollement': '', 'Diagnosis Age': '',
      'screen detail': '', 'Prior Treatment': '', 'Notes': '', 'Hospital Name': '', 'Date of Admission': '', 'Date of Discharge': '', 'Response': '', 'Response Group': '',
      'Response Duration (months)': '', 'Overall Survival (Months)': '', 'Benefit': '', 'date_of_screening': '', 'screen_failure': '', 'hbv_infection_val_mode': '',
      'hbv_infection_code_mode': '', 'duration_hbv_positive': '', 'PREVIOUSLY_SMOKING': '', 'CURRENTLY_SMOKING': ''
    };
    Object.keys(row).forEach(item =>
      obj[item] = row[item]
    );
    return obj;
  }

  createRow(row, key, splitRowKeys, splitNewKey, mergeUnderHeader = null) {
    var obj = {};
    var obj = JSON.parse(JSON.stringify(row));
    obj[splitNewKey] = obj[key];
    (mergeUnderHeader) ? obj[mergeUnderHeader] = key : null;
    splitRowKeys.forEach(function (omitkey) {
      delete obj[omitkey];
    });
    return obj;
  }

  formatCsvFile(inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, fileType, mergeUnderHeader = null) {
    var mainList = [];
    CsvController.parseCsvFile(inFile, fileheader)
      .then((rows) => {
        if (splitRowKeys.length == 0) {
          rows.map(row => {
            if (fileType == 'cytokine') {
              var obj = CsvController.getCytokineRow(row);
              mainList.push(obj);
            }
            if (fileType == 'clinical') {
              var obj = CsvController.getClinicalRow(row);
              mainList.push(obj);
            }
            if (fileType == 'ifc') {
              var obj = CsvController.getIFCRow(row);
              mainList.push(obj);
            }
            if (fileType == 'nanostring') {
              var obj = CsvController.getNanostringRow(row);
              mainList.push(obj);
            }
          });

        } else {
          rows.map(row => {
            for (var i = 0; i < splitRowKeys.length; i++) {
              var obj = CsvController.createRow(row, splitRowKeys[i], splitRowKeys, splitNewKey, mergeUnderHeader);
              mainList.push(obj);
            }
          });
        }
      })
      .then(() => {
        console.log('Started output.');
        CsvController.saveCsvFile(outFile, mainList, outheader)
          .then(() => {
            console.log('Finished processing.');
            //DBController.processsFileCytokine("a","a");
          })
      })
      .catch(err => {
        console.error(err.stack);
        process.exit(1);
      });
  }

  formatCsvFileIFC(inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, fileType, mergeUnderHeader = null) {
    var mainList = [];
    var fType = fileType;
    CsvController.parseCsvFile(inFile, fileheader)
      .then((rows) => {
        if (splitRowKeys.length == 0) {
          var obj = getIFCRow(row);
          mainList.push(obj);
        } else {
          rows.map(row => {
            for (var i = 0; i < splitRowKeys.length; i++) {
              var obj = CsvController.createRow(row, splitRowKeys[i], splitRowKeys, splitNewKey, mergeUnderHeader);
              obj.measure = 'density';
              mainList.push(obj);
            }
          });
        }
      })
      .then(() => {
        console.log('Started output.');
        CsvController.saveCsvFile(outFile, mainList, outheader)
          .then(() => {
            console.log('Finished processing.');
            DBController.processsFileCytokine(fType, outFile);
          })
      })
      .catch(err => {
        console.error(err.stack);
        process.exit(1);
      });
  }

}();


// Cytokine
const convertFileCytokine = async function (inFile, outFile) {
  var fileheader = ['STUDY ID', undefined, undefined, 'SUBJECT ID', 'MARKER', undefined, 'TIMEPOINT', 'RESULT', undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 'predose_ratio', 'median_ratio', 'cycle_pre_per_post', 'cycle', 'median_op_pg_per_ml_cp_', undefined, undefined, 'median_ratio_response', 'median_pgml_response', undefined, undefined, undefined];
  var outheader = ['STUDY ID', 'PHASE', 'ARM', 'TIMEPOINT', 'SAMPLE BARCODE', 'SUBJECT ID', 'PROCEDURE DATE', 'PANEL', 'MARKER', 'MEASURE', 'RESULT', 'RESULT UNIT', 'POPULATION', 'TMM', 'MRN', 'cyclic_loess', 'oracle', 'HG', 'log_transformation', 'CQN', 'RLOG', 'min_max', 'zscore', 'STPM', 'FPKM', 'RPKM', 'RC', 'UQ', 'median', 'RSEM', 'DEGES', 'loggeomeans', 'count', 'log10', 'mean_centering', 'mean_standardization', 'predose_ratio', 'median_ratio', 'cycle_pre_per_post', 'cycle', 'median_op_pg_per_ml_cp_', 'median_ratio_response', 'median_pgml_response'];
  var splitRowKeys = []; //[] if ignored
  var splitNewKey = ''; //'' if ignored
  try {
    CsvController.formatCsvFile(inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'cytokine');  
  } catch (error) {
    console.log(error);
  }  
}

// Convert IFC
const convertFileIFC = async function (inFile, outFile) {
  //'d_studyid','d_usubjid','d_orig_subjid','batch','sample_type','cell_type','slide','cd134_plus_number_per_mmsq','cd134l_plus_number_per_mmsq','cd16_minus_cd56_plus_cd134_plus_number_per_mmsq','cd16_minus_cd56_plus_number_per_mmsq','cd16_plus_cd56_minus_cd134_plus_number_per_mmsq','cd16_plus_cd56_minus_number_per_mmsq','cd16_plus_cd56_plus_cd134_plus_number_per_mmsq','cd16_plus_cd56_plus_number_per_mmsq','cd16_plus_number_per_mmsq','cd3_minus_pdl1_plus_number_per_mmsq','cd3_plus_cd4_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_cd134l_minus_number_per_mmsq','cd3_plus_cd4_plus_cd134l_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_hladr_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_icos_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_icos_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_ki67_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_pd1_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_minus_pd1_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_hladr_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_icos_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_ki67_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_foxp3_plus_pd1_plus_number_per_mmsq','cd3_plus_cd4_plus_icos_plus_number_per_mmsq','cd3_plus_cd4_plus_ki67_plus_cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq','cd3_plus_cd4_plus_ki67_plus_number_per_mmsq','cd3_plus_cd4_plus_number_per_mmsq','cd3_plus_cd4_plus_pdl1_minus_number_per_mmsq','cd3_plus_cd4_plus_pdl1_plus_number_per_mmsq','cd3_plus_cd8_plus_cd134_plus_number_per_mmsq','cd3_plus_cd8_plus_cd134l_minus_number_per_mmsq','cd3_plus_cd8_plus_cd134l_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_cd134_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_hladr_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_icos_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_ki67_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_cd134_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_cd134_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_hladr_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_icos_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_ki67_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq','cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_number_per_mmsq','cd3_plus_cd8_plus_icos_plus_number_per_mmsq','cd3_plus_cd8_plus_ki67_plus_number_per_mmsq','cd3_plus_cd8_plus_number_per_mmsq','cd3_plus_cd8_plus_pdl1_minus_number_per_mmsq','cd3_plus_cd8_plus_pdl1_plus_number_per_mmsq','cd3_plus_icos_minus_number_per_mmsq','cd3_plus_icos_plus_number_per_mmsq','cd3_plus_number_per_mmsq','cd3_plus_pdl1_minus_number_per_mmsq','cd3_plus_pdl1_plus_number_per_mmsq','cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq','cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq','cd4_plus_number_per_mmsq','cd56_plus_number_per_mmsq','cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq','cd8_plus_number_per_mmsq','foxp3_plus_number_per_mmsq','granzymeb_plus_number_per_mmsq','hladr_plus_number_per_mmsq','icos_plus_cd134_plus_number_per_mmsq','icos_plus_number_per_mmsq','ki67_plus_number_per_mmsq','panck_plus_number_per_mmsq','pd1_plus_cd134_plus_number_per_mmsq','pd1_plus_number_per_mmsq','pdl1_minus_number_per_mmsq','pdl1_plus_number_per_mmsq','acc_number_cont_number','patient_number','pre_post','tissue_type','batch_description','part','cohort_1','cohort_2','dose_mg_kg','dose_op_mg_per_kg_cp_','total_cycles','tumor_type','tumor','tumor_detail','tissue_source','visit_description','visit_date','actual_dose_1','actual_dose_2','actual_dose_3','smart_specimen_class_description','pd_l_1_status_site','pd_op_l_cp_1_status_op_site_cp_','prior_pd_1','prior_pd1_qstn_','prior_ipi','prior_ipi_qstn_','prior_smoking','prior_smoking_qstn_','current_smoker','current_smoker_qstn_','effusion','history_of_effusion','lung_involvement','status2','therapy_type','response','d_subjid','d_cohort','d_visit_name','d_timepoint','timepoint1','pre_post'
  var fileheader = ['study_id', undefined, 'SUBJECT ID', undefined, undefined, 'tissue_category', 'Slide ID', 'cd134_plus_number_per_mmsq', 'cd134l_plus_number_per_mmsq', 'cd16_minus_cd56_plus_cd134_plus_number_per_mmsq', 'cd16_minus_cd56_plus_number_per_mmsq',
    'cd16_plus_cd56_minus_cd134_plus_number_per_mmsq', 'cd16_plus_cd56_minus_number_per_mmsq', 'cd16_plus_cd56_plus_cd134_plus_number_per_mmsq', 'cd16_plus_cd56_plus_number_per_mmsq', 'cd16_plus_number_per_mmsq',
    'cd3_minus_pdl1_plus_number_per_mmsq', 'cd3_plus_cd4_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd4_plus_cd134l_minus_number_per_mmsq', 'cd3_plus_cd4_plus_cd134l_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_cd134_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_minus_hladr_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_minus_icos_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_icos_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_minus_ki67_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_minus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_pd1_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_minus_pd1_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_hladr_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_plus_icos_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_ki67_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd4_plus_foxp3_plus_pd1_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_icos_plus_number_per_mmsq', 'cd3_plus_cd4_plus_ki67_plus_cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd4_plus_ki67_plus_number_per_mmsq', 'cd3_plus_cd4_plus_number_per_mmsq', 'cd3_plus_cd4_plus_pdl1_minus_number_per_mmsq', 'cd3_plus_cd4_plus_pdl1_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd8_plus_cd134l_minus_number_per_mmsq', 'cd3_plus_cd8_plus_cd134l_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_minus_cd134_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_minus_hladr_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_icos_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_minus_ki67_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_minus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_cd134_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_hladr_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_plus_icos_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_plus_ki67_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_plus_number_per_mmsq', 'cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_number_per_mmsq', 'cd3_plus_cd8_plus_icos_plus_number_per_mmsq', 'cd3_plus_cd8_plus_ki67_plus_number_per_mmsq', 'cd3_plus_cd8_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_pdl1_minus_number_per_mmsq', 'cd3_plus_cd8_plus_pdl1_plus_number_per_mmsq', 'cd3_plus_icos_minus_number_per_mmsq', 'cd3_plus_icos_plus_number_per_mmsq', 'cd3_plus_number_per_mmsq',
    'cd3_plus_pdl1_minus_number_per_mmsq', 'cd3_plus_pdl1_plus_number_per_mmsq', 'cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq', 'cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd4_plus_number_per_mmsq', 'cd56_plus_number_per_mmsq', 'cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq', 'cd8_plus_number_per_mmsq', 'foxp3_plus_number_per_mmsq', 'granzymeb_plus_number_per_mmsq',
    'hladr_plus_number_per_mmsq', 'icos_plus_cd134_plus_number_per_mmsq', 'icos_plus_number_per_mmsq', 'ki67_plus_number_per_mmsq', 'panck_plus_number_per_mmsq', 'pd1_plus_cd134_plus_number_per_mmsq', 'pd1_plus_number_per_mmsq',
    'pdl1_minus_number_per_mmsq', 'pdl1_plus_number_per_mmsq', undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 'timepoint', undefined, undefined, undefined];

  //'study_id','Phase','ARM', 'timepoint', 'SUBJECT ID', 'SPECIMEN', 'Slide  Label ', 'Slide ID', 'tissue_category', 'tissue_area', 'marker', 'measure', 'grade', 'result', 'result_unit', 'TMM', 'MRN', 'cyclic_loess', 'oracle', 'HG', 'log_transformation', 'CQN', 'RLOG', 'min_max', 'zscore', 'STPM', 'FPKM', 'RPKM', 'RC', 'UQ', 'median', 'RSEM', 'DEGES', 'loggeomeans', 'count', 'log10', 'mean_centering', 'mean_standardisation'
  var outheader = ['study_id', 'Phase', 'ARM', 'timepoint', 'SUBJECT ID', 'SPECIMEN', 'Slide  Label ', 'Slide ID', 'tissue_category', 'tissue_area', 'marker', 'measure', 'grade', 'result', 'result_unit', 'TMM', 'MRN', 'cyclic_loess', 'oracle', 'HG', 'log_transformation', 'CQN', 'RLOG', 'min_max', 'zscore', 'STPM', 'FPKM', 'RPKM', 'RC', 'UQ', 'median', 'RSEM', 'DEGES', 'loggeomeans', 'count', 'log10', 'mean_centering', 'mean_standardisation'];

  var splitRowKeys = [
    'cd134_plus_number_per_mmsq',
    'cd134l_plus_number_per_mmsq',
    'cd16_minus_cd56_plus_cd134_plus_number_per_mmsq',
    'cd16_minus_cd56_plus_number_per_mmsq',
    'cd16_plus_cd56_minus_cd134_plus_number_per_mmsq',
    'cd16_plus_cd56_minus_number_per_mmsq',
    'cd16_plus_cd56_plus_cd134_plus_number_per_mmsq',
    'cd16_plus_cd56_plus_number_per_mmsq',
    'cd16_plus_number_per_mmsq',
    'cd3_minus_pdl1_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_cd134l_minus_number_per_mmsq',
    'cd3_plus_cd4_plus_cd134l_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_hladr_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_icos_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_icos_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_ki67_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_minus_pd1_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_hladr_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_icos_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_ki67_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_foxp3_plus_pd1_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_icos_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_ki67_plus_cd3_plus_cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_ki67_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_number_per_mmsq',
    'cd3_plus_cd4_plus_pdl1_minus_number_per_mmsq',
    'cd3_plus_cd4_plus_pdl1_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_cd134l_minus_number_per_mmsq',
    'cd3_plus_cd8_plus_cd134l_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_hladr_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_icos_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_ki67_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_minus_pd1_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_hladr_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_icos_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_ki67_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_granzymeb_plus_pd1_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_icos_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_ki67_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_number_per_mmsq',
    'cd3_plus_cd8_plus_pdl1_minus_number_per_mmsq',
    'cd3_plus_cd8_plus_pdl1_plus_number_per_mmsq',
    'cd3_plus_icos_minus_number_per_mmsq',
    'cd3_plus_icos_plus_number_per_mmsq',
    'cd3_plus_number_per_mmsq',
    'cd3_plus_pdl1_minus_number_per_mmsq',
    'cd3_plus_pdl1_plus_number_per_mmsq',
    'cd4_plus_foxp3_plus_icos_plus_cd134_plus_number_per_mmsq',
    'cd4_plus_foxp3_plus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd4_plus_number_per_mmsq',
    'cd56_plus_number_per_mmsq',
    'cd8_plus_granzymeb_plus_pd1_plus_cd134_plus_number_per_mmsq',
    'cd8_plus_number_per_mmsq',
    'foxp3_plus_number_per_mmsq',
    'granzymeb_plus_number_per_mmsq',
    'hladr_plus_number_per_mmsq',
    'icos_plus_cd134_plus_number_per_mmsq',
    'icos_plus_number_per_mmsq',
    'ki67_plus_number_per_mmsq',
    'panck_plus_number_per_mmsq',
    'pd1_plus_cd134_plus_number_per_mmsq',
    'pd1_plus_number_per_mmsq',
    'pdl1_minus_number_per_mmsq',
    'pdl1_plus_number_per_mmsq'
  ]; //[] if ignored
  var splitNewKey = 'result'; //'' if ignored
  CsvController.formatCsvFileIFC(inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'ifc', 'marker');
}

// Convert Nanostring file
const convertFileNanostring = async function (inFile, outFile) {
  //'study_id','d_studyid','subject_number','d_orig_subjid','d_usubjid','specimen_id','visit','lims_id','sample_id','gene_name','class_name','standard_result_numeric','d_subjid','d_visit_name','d_timepoint'
  var fileheader = [undefined, 'STUDY ID', undefined, 'SUBJECT ID', undefined, undefined, undefined, undefined, 'SPECIMEN', 'PROBE', undefined, 'RAW_VALUE', undefined, 'TIMEPOINT', undefined];
  //'PROBE','STUDY ID','PHASE','ARM','TIMEPOINT','SUBJECT ID','SPECIMEN','RAW_VALUE','TMM','MRN','cyclic_loess','oracle','HG','log_transformation','CQN','RLOG','min_max','zscore','STPM','FPKM','RPKM','RC','UQ','median','RSEM','DEGES','loggeomeans','count','log10','mean_centering','mean_standardization'
  var outheader = ['PROBE', 'STUDY ID', 'PHASE', 'ARM', 'TIMEPOINT', 'SUBJECT ID', 'SPECIMEN', 'RAW_VALUE', 'TMM', 'MRN', 'cyclic_loess', 'oracle', 'HG', 'log_transformation', 'CQN', 'RLOG', 'min_max', 'zscore', 'STPM', 'FPKM', 'RPKM', 'RC', 'UQ', 'median', 'RSEM', 'DEGES', 'loggeomeans', 'count', 'log10', 'mean_centering', 'mean_standardization'];
  var splitRowKeys = []; //[] if ignored
  var splitNewKey = ''; //'' if ignored
  CsvController.formatCsvFile(inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'nanostring');
}

// ihc
const convertFileIhc = async function (inFile, outFile) {
  var fileheader = ['STUDY ID', undefined, undefined, undefined, 'SUBJECT ID', 'SAMPLE BARCODE', 'CLIENT SAMPLE BARCODE', undefined, 'MARKER',
    undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, 'combined_positive_score',
    'tumor_fractional_intensity_cln_prct_0', 'tumor_fractional_intensity_cln_prct_1_pls_', 'tumor_fractional_intensity_cln_prct_2_pls_',
    'tumor_fractional_intensity_cln_prct_3_pls_', 'tumor_proportion_score', 'aretaicspresent', 'density_op_mids_cp_', 'mean_intensity_op_mimi_cp_',
    undefined, undefined, undefined];

  var outheader = ['STUDY ID', 'PHASE', 'ARM', 'TIMEPOINT', 'SAMPLE BARCODE', 'CLIENT SAMPLE BARCODE', 'SUBJECT ID', 'PROCEDURE DATE', 'MARKER', 'MEASURE',
    'TISSUE', 'tissue_area', 'grade', 'region', 'RESULT', 'RESULT UNIT', 'TMM', 'MRN', 'cyclic_loess', 'oracle', 'HG', 'log_transformation', 'CQN', 'RLOG', 'min_max',
    'zscore', 'count', 'RC', 'UQ', 'median', 'RSEM', 'STPM', 'FPKM', 'RPKM', 'DEGES', 'loggeomeans', 'log10', 'mean_centering', 'mean_standardization'];

  var splitRowKeys = ['combined_positive_score', 'tumor_fractional_intensity_cln_prct_0', 'tumor_fractional_intensity_cln_prct_1_pls_',
    'tumor_fractional_intensity_cln_prct_2_pls_', 'tumor_fractional_intensity_cln_prct_3_pls_', 'tumor_proportion_score',
    'density_op_mids_cp_', 'mean_intensity_op_mimi_cp_']; //[] if ignored

  var splitNewKey = 'RESULT';
  CsvController.formatCsvFile(inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'ihc', 'MEASURE');
}

// cytometry
const convertFileCytometry = async function (inFile, outFile) {
  var fileheader = ['STUDY ID', undefined, undefined, 'SUBJECT ID', undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    'PANEL', 'MARKER', undefined, 'iso_value', 'iso_units', undefined, 'si_value', 'si_units', undefined, 'other_value', 'other_units',
    undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, 'd_timepoint'];

  var outheader = ['STUDY ID', 'PHASE', 'ARM', 'TIMEPOINT', 'SAMPLE BARCODE', 'SUBJECT ID', 'PROCEDURE DATE', 'PANEL', 'MARKER', 'MEASURE', 'RESULT', 'RESULT UNIT',
    'Parent Population', 'POPULATION', 'TMM', 'MRN', 'cyclic_loess', 'oracle', 'HG', 'log_transformation', 'CQN', 'RLOG', 'min_max', 'zscore', 'COUNT', 'RC', 'UQ', 'MEDIAN',
    'RSEM', 'STPM', 'FPKM', 'RPKM', 'DEGES', 'LOGGEOMEANS', 'LOG10', 'MEAN_CENTERING', 'MEAN_STANDARDIZATION'];

  var splitRowKeys = ['iso_value', 'si_value', 'other_value']; //[] if ignored
  var splitNewKey = 'RESULT';
  CsvController.formatCsvFile(inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'cytometry', 'MEASURE');
}

// clinical
const convertFileClinical = async function(inFile, outFile) {
  var fileheader = ['Study ID', undefined, 'Patient ID', 'New Arm', 'Cohort Group', 'Response', undefined, undefined, undefined, 'PREVIOUSLY_SMOKING', 'CURRENTLY_SMOKING', undefined,
    'Primary Tumor/Cancer Type', 'disease detail', 'Disease Stage at Enrollement', undefined, undefined, undefined, undefined, undefined, undefined, undefined];

  var outheader = ['First Name', 'Middle Name', 'Last Name1', 'Last Name2', 'Patient ID', 'height', 'weight', 'bmi', 'Sex', 'Race', 'ssn', 'dob', 'dod',
    'marital status', 'address1', 'address2', 'city', 'state', 'country', 'zip_code', 'email', 'phone', 'fax', 'photo', 'ethnicity', 'nationality',
    'Overall Survival Status', 'Study ID', 'PHASE', 'New Arm', 'Cohort Group', 'Enrolling Org', 'Enrolling Date', 'Concent Date', 'client_suid', 'GENE Of Intrest1',
    'GENE Of Intrest1 Mutation ', 'GENE Of Intrest2', 'GENE Of Intrest2 mutation', 'timepoint', 'Primary Tumor/Cancer Type', 'disease detail',
    'Disease Stage at Enrollement', 'Diagnosis Age', 'screen detail', 'Prior Treatment', 'Notes', 'Hospital Name', 'Date of Admission', 'Date of Discharge',
    'Response', 'Response Group', 'Response Duration (months)', 'Overall Survival (Months)', 'Benefit', 'date_of_screening', 'screen_failure',
    'hbv_infection_val_mode', 'hbv_infection_code_mode', 'duration_hbv_positive', 'PREVIOUSLY_SMOKING', 'CURRENTLY_SMOKING'];

  var splitRowKeys = []; //[] if ignored
  var splitNewKey = '';
  CsvController.formatCsvFile(inFile, outFile, fileheader, outheader, splitRowKeys, splitNewKey, 'clinical');
}

const convertCSVController = {
  'convertFileCytokine': convertFileCytokine,
  'convertFileIFC': convertFileIFC,
  'convertFileNanostring': convertFileNanostring,
  'convertFileIhc': convertFileIhc,
  'convertFileCytometry': convertFileCytometry,
  'convertFileClinical': convertFileClinical
};

module.exports = convertCSVController;