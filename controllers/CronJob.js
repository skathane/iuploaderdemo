var CronJob = require('cron').CronJob;
var appDir = require('path').dirname(require.main.filename);
var config = require('config');
var jobController = require(`./JobController`);


var startCronJob = function () {
  console.log("Start Cron Job");
  new CronJob(config.CRONJOB_POLICY, function () {  // CRONJOB_POLICY format (seconds minutes hours DayOfMonth Month DayOfWeek)
    console.log("Cron Job Executing");
    jobController.getJob();
  }, null, true, config.CRONJOB_TIMEZONE);
}


var cronJobController = {
  'startCronJob': startCronJob
};

module.exports = cronJobController;
